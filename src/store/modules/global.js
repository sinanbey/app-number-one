// initial state
const state = {
    screenOrientationLandscape: false
};

// getters
const getters = {};

// actions
const actions = {
    setScreenOrientation({commit}, screenOrientation) {
        commit('setScreenOrientation', (screenOrientation.type === 'landscape-primary' || screenOrientation.type === 'landscape'));
    },
};

// mutations
const mutations = {
    setScreenOrientation(state, screenOrientation) {
        state.screenOrientationLandscape = screenOrientation;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}