// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'babel-polyfill'
import store from './store'

import Currencies from './components/currency/Currencies';
import CurrencyCalculator from './components/currency/CurrencyCalculator';
import CurrencyList from './components/currency/CurrencyList';
import CurrencyFavoritesList from './components/currency/CurrencyFavoritesList';

import Stocks from './components/stock/Stocks';
import StockList from './components/stock/StockList';
import StockListItem from './components/stock/StockListItem';
import StockSearch from './components/stock/StockSearch';
import StockDetailsCompany from './components/stock/StockDetailsCompany';
import StockDetailsChart from './components/stock/StockDetailsChart';
import StockFavoriteButton from './components/stock/StockFavoriteButton';
import StockNewsList from './components/stock/StockNewsList';

import VueHighcharts from 'vue-highcharts';
import Highcharts from 'highcharts/highstock';

import './css/app.css'

const colors = {
    primary: {
        base: '#26C8C4',
        light: '#6dfcf7',
        dark: '#009794',
    }
};

Vue.use(VueHighcharts, {Highcharts});
Vue.use(Vuex);

Vue.component('currencies', Currencies);
Vue.component('currency-calculator', CurrencyCalculator);
Vue.component('currency-list', CurrencyList);
Vue.component('currency-favorites-list', CurrencyFavoritesList);

Vue.component('stocks', Stocks);
Vue.component('stock-list', StockList);
Vue.component('stock-list-item', StockListItem);

Vue.component('stock-search', StockSearch);
Vue.component('stock-details-company', StockDetailsCompany);
Vue.component('stock-favorite-button', StockFavoriteButton);
Vue.component('stock-details-chart', StockDetailsChart);

Vue.component('stock-news-list', StockNewsList);

Vue.use(
    Vuetify, {
        theme: {
            primary: colors.primary.base,
        }
    },
);
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    data: {},
    mounted() {
        this.$nextTick(function () {
            window.addEventListener("orientationchange", this.setScreenOrientation);
            //Init
            this.setScreenOrientation();
        })
    },
    methods: {
        setScreenOrientation() {
            this.$store.dispatch('global/setScreenOrientation', screen.orientation);
        },

    },
    destroyed() {
        window.removeEventListener('orientationchange', this.setScreenOrientation)
    },
    components: {
        App
    },
    template: '<App/>',
});
