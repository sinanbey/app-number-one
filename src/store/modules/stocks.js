import axios from 'axios';
import Vue from 'vue';
import localforage from 'localforage';

const stocksLocalforage = localforage.createInstance({
    name: 'stocks'
});

const baseUrl = "https://api.iextrading.com/1.0";

// initial state
const state = {
    gainerList: [],
    loserList: [],
    details: {
        company: {},
        news: {},
        peers: {},
        logo: '',
        chart: {},
    },
    favoritesQuotes: {},
    gainersListError: null,
    losersListError: null,
    detailsError: null,
    chartError: null,
    loading: 0,
};

// getters
const getters = {};

// actions
const actions = {
    getGainersList({commit}) {
        let apiFirst = false;
        const dbKey = 'gainersList';

        commit('setLoading');

        stocksLocalforage.getItem(dbKey).then(function (value) {
            if (!apiFirst) {
                console.debug('get ' + dbKey + ' from Cache');
                commit('setGainersList', value);
            }
        }).catch(function (err) {
            console.log(err);
        });

        axios.get(baseUrl + '/stock/market/list/gainers/quote?displayPercent=true')
            .then(function (response) {
                if (response.data) {
                    return response.data;
                }
                return Promise.reject({request: dbKey, message: response.error});
            })
            .then(function (result) {
                apiFirst = true;
                console.debug('get ' + dbKey + ' from API');
                commit('setGainersList', result);
                commit('clearError', dbKey);
                return result;
            })
            .then(function (result) {
                stocksLocalforage.setItem(dbKey, result)
                    .catch(function (err) {
                        console.log(err);
                    });
            })
            .catch(function (error) {
                return commit('setError', error);
            })
            .finally(function () {
                commit('removeLoading');
            });
    },
    getLosersList({commit}) {
        let apiFirst = false;
        const dbKey = 'losersList';

        commit('setLoading');

        stocksLocalforage.getItem(dbKey).then(function (value) {
            if (!apiFirst) {
                console.debug('get ' + dbKey + ' from Cache');
                commit('setLosersList', value);
            }
        }).catch(function (err) {
            console.log(err);
        });

        axios.get(baseUrl + '/stock/market/list/losers/quote?displayPercent=true')
            .then(function (response) {
                if (response.data) {
                    return response.data;
                }
                return Promise.reject({request: dbKey, message: response.error});
            })
            .then(function (result) {
                apiFirst = true;
                console.debug('get ' + dbKey + ' from API');
                commit('setLosersList', result);
                commit('clearError', dbKey);
                return result;
            })
            .then(function (result) {
                stocksLocalforage.setItem(dbKey, result)
                    .catch(function (err) {
                        console.log(err);
                    });
            })
            .catch(function (error) {
                return commit('setError', error);
            })
            .finally(function () {
                commit('removeLoading');
            });
    },
    getDetails({commit}, symbol) {
        let apiFirst = false;
        const dbKey = 'details-' + symbol;

        commit('setLoading');

        stocksLocalforage.getItem(dbKey).then(function (value) {
            if (!apiFirst) {
                console.debug('get ' + dbKey + ' from Cache');
                commit('setDetails', value);
            }
        }).catch(function (err) {
            console.log(err);
        });

        axios.get(baseUrl + '/stock/' + symbol + '/batch?types=company,news,peers,logo,quote')
            .then(function (response) {
                if (response.data) {
                    return response.data;
                }
                return Promise.reject({request: 'details', message: response.error});
            })
            .then(function (result) {
                const peers = result.peers;
                if (Object.keys(peers).length === 0) {
                    return Promise.resolve(result);
                }
                return axios.get(baseUrl + '/stock/market/batch?symbols=' + peers.toString() + '&types=quote')
                    .then(function (response) {
                        let peersResult = {};
                        if (response.data) {
                            Object.entries(response.data).forEach(([key, value]) => {
                                peersResult[key] = value.quote;
                            });
                            result.peers = peersResult;
                            return result;
                        }
                        return Promise.reject({request: 'details', message: response.error});
                    });
            })
            .then(function (result) {
                apiFirst = true;
                console.debug('get ' + dbKey + ' from API');
                commit('setDetails', result);
                commit('clearError', 'details');
                return result;
            })
            .then(function (result) {
                stocksLocalforage.setItem(dbKey, result)
                    .catch(function (err) {
                        console.log(err);
                    });
            })
            .catch(function (error) {
                return commit('setError', error);
            })
            .finally(function () {
                commit('removeLoading');
            });
    },
    getFavoritesQuotes({commit}, symbols) {
        let apiFirst = false;
        const dbKey = 'favoritesQuotes';

        commit('setLoading');

        stocksLocalforage.getItem(dbKey).then(function (value) {
            if (!apiFirst) {
                console.debug('get ' + dbKey + ' from Cache');
                commit('setFavoritesQuotes', value);
            }
        }).catch(function (err) {
            console.log(err);
        });

        axios.get(baseUrl + '/stock/market/batch?symbols=' + symbols.toString() + '&types=quote')
            .then(function (response) {
                let result = {};
                if (response.data) {
                    Object.entries(response.data).forEach(([key, value]) => {
                        result[key] = value.quote;
                    });
                    return result;
                }
                return Promise.reject({request: dbKey, message: response.error});
            })
            .then(function (result) {
                apiFirst = true;
                console.debug('get ' + dbKey + ' from API');
                commit('setFavoritesQuotes', result);
                commit('clearError', dbKey);
                return result;
            })
            .then(function (result) {
                stocksLocalforage.setItem(dbKey, result)
                    .catch(function (err) {
                        console.log(err);
                    });
            })
            .catch(function (error) {
                return commit('setError', error);
            })
            .finally(function () {
                commit('removeLoading');
            });
    },
    getChart({commit}, request) {
        let apiFirst = false;
        const dbKey = 'chart-' + request.symbol + '-' + request.range;

        commit('setLoading');

        stocksLocalforage.getItem(dbKey).then(function (value) {
            if (!apiFirst) {
                console.debug('get ' + dbKey + ' from Cache');
                commit('setChart', value);
            }
        }).catch(function (err) {
            console.log(err);
        });

        axios.get(baseUrl + '/stock/' + request.symbol + '/chart/' + request.range)
            .then(function (response) {
                if (response.data) {
                    return {data: response.data, range: request.range};
                }
                return Promise.reject({request: 'chart', message: response.error});
            })
            .then(function (result) {
                apiFirst = true;
                console.debug('get ' + dbKey + ' from API');
                commit('setChart', result);
                commit('clearError', 'chart');
                return result;
            })
            .then(function (result) {
                stocksLocalforage.setItem(dbKey, result)
                    .catch(function (err) {
                        console.log(err);
                    });
            })
            .catch(function (error) {
                return commit('setError', error);
            })
            .finally(function () {
                commit('removeLoading');
            });
    },
};

// mutations
const mutations = {
    setGainersList(state, stocks) {
        state.gainerList = stocks
    },
    setLosersList(state, stocks) {
        state.loserList = stocks
    },
    setDetails(state, value) {
        state.details = value
    },
    setFavoritesQuotes(state, value) {
        state.favoritesQuotes = value;
    },
    setChart(state, value) {
        Vue.set(state.details, 'chart', value);
    },
    setLoading(state) {
        state.loading++;
    },
    removeLoading(state) {
        state.loading--;
    },
    setError(state, error) {
        console.debug(error);
        switch (error.request) {
            case 'gainersList':
                state.gainersListError = error.message;
                break;
            case 'losersList':
                state.losersListError = error.message;
                break;
            case 'details':
                state.favoritesRatesError = error.message;
                break;
            case 'chart':
                state.chartError = error.message;
                break;
            default:
                state.gainersListError = error.message;
                state.losersListError = error.message;
                state.detailsError = error.message;
                state.chartError = error.message;
        }
    },
    clearError(state, request) {
        switch (request) {
            case 'gainersList':
                state.gainersListError = '';
                break;
            case 'losersList':
                state.losersListError = '';
                break;
            case 'details':
                state.detailsError = '';
                break;
            case 'chart':
                state.chartError = '';
                break;
            default:
                state.gainersListError = '';
                state.losersListError = '';
                state.detailsError = '';
                state.chartError = '';
        }
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}