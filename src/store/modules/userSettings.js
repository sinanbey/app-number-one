import Vue from 'vue';
import localforage from 'localforage';

const userSettingsLocalforage = localforage.createInstance({
    name: 'userSettings'
});

// initial state
const state = {
    app: {
        darkTheme : true,
    },
    currency: {
        favoritesListSettings: {},
        favorites: {},
        calculatorSettings: {
            baseCurrencySelected: null,
            targetCurrencySelected: null
        },
    },
    stock: {
        favorites: {},
        searchHistory: {},
    }
};

// getters
const getters = {};

// actions
const actions = {
    getUserSettings({commit, state}, dbKey) {
        if (!state.hasOwnProperty(dbKey)) {
            return console.debug('UserSettings dbKey: ' + dbKey + 'not found!');
        }
        userSettingsLocalforage.getItem(dbKey).then(function (value) {
            if (!value) {
                //set first value
                value = state[dbKey];
            }
            console.debug('get userSettings ' + dbKey + ' from Cache', value);
            commit('setUserSettings', {dbKey: dbKey, value: value});
        }).catch(function (err) {
            console.debug(err);
        });
    },
    setUserSettings({commit, state}, data) {
        if (!state.hasOwnProperty(data.dbKey)) {
            return console.debug('UserSettings dbKey: ' + dbKey + 'not found!');
        }
        const newData = {};
        Object.assign(newData, state[data.dbKey], data.value);

        userSettingsLocalforage.setItem(data.dbKey, newData).then(function (value) {
            commit('setUserSettings', {dbKey: data.dbKey, value: value});
        }).catch(function (err) {
            console.debug(err);
        });
    }
};

// mutations
const mutations = {
    setUserSettings(state, data) {
        state[data.dbKey] = data.value;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}