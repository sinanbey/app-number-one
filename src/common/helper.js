import moment from 'moment';

function formatCurrency(value, isoCode) {
    if (value && isoCode) {
        return new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: isoCode,
            minimumFractionDigits: 2,
            maximumFractionDigits: 5
        }).format(value);
    }
}

function formatDateTime(value) {
    const dateTime = moment(value);
    if (dateTime.isValid()) {
        return dateTime.format("YYYY-MM-DD HH:mm");
    }
    return '';
}

function chartTimeToUnix(date) {
    const dateTime = moment(date);
    if (dateTime.isValid()) {
        return dateTime.valueOf();
    }
    return '';
}

function chartTime1dToUnix(date, minute) {
    const dateTime = moment(date + ' ' + minute, "YYYYMMDD HH:mm");
    if (dateTime.isValid()) {
        return dateTime.valueOf();
    }
    return '';
}


export default {
    namespaced: true,
    formatCurrency,
    formatDateTime,
    chartTimeToUnix,
    chartTime1dToUnix
}