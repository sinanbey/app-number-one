import Vue from 'vue'
import Vuex from 'vuex'
import global from './modules/global'
import userSettings from './modules/userSettings'
import currencies from './modules/currencies'
import stocks from './modules/stocks'
import createLogger from 'vuex/dist/logger'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        global,
        userSettings,
        currencies,
        stocks,
    },
    strict: debug,
    plugins: debug ? [createLogger()] : []
})