import axios from 'axios';
import Vue from 'vue';
import localforage from 'localforage';

const currenciesLocalforage = localforage.createInstance({
    name: 'currencies'
});

const baseUrl = "https://www.app-number.one/api/fixer.php/";

// initial state
const state = {
    list: [],
    favoritesRates: {},
    calculatorRates: {},
    listError: null,
    calculatorRatesError: null,
    favoritesRatesError: null,
    loading: 0,
};

// getters
const getters = {};

// actions
const actions = {
    getList({commit}) {
        let apiFirst = false;
        const dbKey = 'list';

        commit('setLoading');

        currenciesLocalforage.getItem(dbKey).then(function (value) {
            if (!apiFirst) {
                console.debug('get ' + dbKey + ' from Cache');
                commit('setList', value);
            }
        }).catch(function (err) {
            console.log(err);
        });

        axios.get(baseUrl + 'symbols')
            .then(function (response) {
                let result = {};
                if (response.data && response.data.success) {
                    Object.entries(response.data.symbols).forEach(([isoCode, name]) => {
                        result[isoCode] = {
                            "name": name,
                            "isoCode": isoCode
                        }
                    });
                    return result;
                }
                return Promise.reject({request: dbKey, message: response.data.error.type});
            })
            .then(function (result) {
                apiFirst = true;
                console.debug('get ' + dbKey + ' from API');
                commit('setList', result);
                commit('clearError', dbKey);
                return result;
            })
            .then(function (result) {
                currenciesLocalforage.setItem(dbKey, result)
                    .catch(function (err) {
                        console.log(err);
                    });
            })
            .catch(function (error) {
                return commit('setError', error);
            })
            .finally(function () {
                commit('removeLoading');
            });
    },
    getExchangeRate({commit}, currencies) {
        let apiFirst = false;
        const dbKey = 'calculatorRates';

        commit('setLoading');

        currenciesLocalforage.getItem(dbKey).then(function (value) {
            if (!apiFirst) {
                console.debug('get CurrenciesList from Cache');
                commit('setCalculatorRates', value);
            }
        }).catch(function (err) {
            console.log(err);
        });

        axios.get(baseUrl + 'latest&base=' + currencies.baseCurrency + '&symbols=' + currencies.targetCurrency)
            .then(function (response) {
                if (response.data && response.data.success) {
                    const rates = response.data.rates;
                    return {
                        baseCurrency: response.data.base,
                        timestamp: response.data.timestamp,
                        targetCurrency: Object.keys(rates)[0],
                        targetRate: rates[Object.keys(rates)[0]]
                    };
                }
                return Promise.reject({request: dbKey, message: response.data.error.type});
            })
            .then(function (result) {
                apiFirst = true;
                console.debug('get ' + dbKey + ' from API');
                commit('setCalculatorRates', result);
                commit('clearError', dbKey);
                return result;
            })
            .then(function (result) {
                currenciesLocalforage.setItem(dbKey, result)
                    .catch(function (err) {
                        console.log(err);
                    });
            })
            .catch(function (error) {
                return commit('setError', error);
            })
            .finally(function () {
                commit('removeLoading');
            });
    },
    getExchangeRates({commit}, currencies) {
        let apiFirst = false;
        const dbKey = 'favoritesRates';

        commit('setLoading');

        currenciesLocalforage.getItem(dbKey).then(function (value) {
            if (!apiFirst) {
                commit('setFavoritesRates', value);
            }
        }).catch(function (err) {
            console.log(err);
        });

        axios.get(baseUrl + 'latest&base=' + currencies.baseCurrency + '&symbols=' + currencies.targetCurrencies.join(','))
            .then(function (response) {
                if (response.data && response.data.success) {
                    return {
                        baseCurrency: response.data.base,
                        timestamp: response.data.timestamp,
                        targetRates: response.data.rates
                    };
                }
                return Promise.reject({request: dbKey, message: response.data.error.type});
            })
            .then(function (result) {
                apiFirst = true;
                console.debug('get ' + dbKey + ' from API');
                commit('setFavoritesRates', result);
                commit('clearError', dbKey);
                return result;
            })
            .then(function (result) {
                currenciesLocalforage.setItem(dbKey, result)
                    .catch(function (err) {
                        console.log(err);
                    });
            })
            .catch(function (error) {
                return commit('setError', error);
            })
            .finally(function () {
                commit('removeLoading');
            });
    }
};

// mutations
const mutations = {
    setList(state, currencies) {
        state.list = currencies
    },
    setCalculatorRates(state, rate) {
        const key = rate.baseCurrency + '_' + rate.targetCurrency;
        Vue.set(state.calculatorRates, key, rate);
    },
    setFavoritesRates(state, rates) {
        Vue.set(state.favoritesRates, rates.baseCurrency, rates);
    },
    setLoading(state) {
        state.loading++;
    },
    removeLoading(state) {
        state.loading--;
    },
    setError(state, error) {
        console.debug(error);
        switch (error.request) {
            case 'list':
                state.listError = error.message;
                break;
            case 'calculatorRates':
                state.calculatorRatesError = error.message;
                break;
            case 'favoritesRates':
                state.favoritesRatesError = error.message;
                break;
            default:
                state.listError = error.message;
                state.calculatorRatesError = error.message;
                state.favoritesRatesError = error.message;
        }
    },
    clearError(state, request) {
        switch (request) {
            case 'list':
                state.listError = '';
                break;
            case 'calculatorRates':
                state.calculatorRatesError = '';
                break;
            case 'favoritesRates':
                state.favoritesRatesError = '';
                break;
            default:
                state.listError = '';
                state.calculatorRatesError = '';
                state.favoritesRatesError = '';
        }
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}