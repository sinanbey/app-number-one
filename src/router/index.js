import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import PrivacyPolicy from '@/components/common/PrivacyPolicy'
import LegalNotice from '@/components/common/LegalNotice'

import Currencies from '@/components/currency/Currencies'
import CurrencyCalculator from '@/components/currency/CurrencyCalculator'

import Stocks from '@/components/stock/Stocks'
import StockDetails from '@/components/stock/StockDetails'

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: HelloWorld
        },
        {
            path: '*',
            name: 'home',
            component: HelloWorld
        },
        {
            path: '/privacy-policy',
            name: 'privacyPolicy',
            component: PrivacyPolicy
        },
        {
            path: '/legal-notice',
            name: 'legalNotice',
            component: LegalNotice
        },
        {
            path: '/currencies',
            name: 'currencies',
            component: Currencies
        },
        {
            path: '/currency-calculator',
            name: 'currencyCalculator',
            component: CurrencyCalculator
        },
        {
            path: '/stocks',
            name: 'stocks',
            component: Stocks
        },
        {
            path: '/stock/:symbol',
            name: 'stockDetails',
            component: StockDetails,
            meta: { showBackButton: true }
        }
    ]
})
